
class AdditivesController {

  static String getAdditiveFunction(String additive) {
    int number = int.parse(additive.replaceAll(RegExp(r'[^0-9]+'),''));

    if(number <= 180) {
      return "Colorant";
    } else if(number >= 200 && number <= 297) {
      return "Conservateur";
    } else if(number >= 300 && number <= 321) {
      return "Antioxydant";
    } else if(number > 321 && number <= 322) {
      return "Lécithine";
    } else if(number >= 325 && number <= 327) {
      return "Lactate";
    } else if(number >= 330 && number <= 333) {
      return "Citrate";
    } else if(number >= 334 && number <= 337) {
      return "Tartrate";
    } else if(number >= 338 && number <= 343) {
      return "Orthophosphate";
    } else if(number >= 350 && number <= 352) {
      return "Malate";
    } else if(number >= 353 && number <= 354) {
      return "Tartrate";
    } else if(number >= 355 && number <= 392) {
      return "Adipate";
    } else if(number >= 400 && number <= 409) {
      return "Alginate";
    } else if(number >= 410 && number <= 446) {
      return "Gomme";
    } else if(number >= 450 && number <= 450) {
      return "Diphosphate";
    } else if(number >= 451 && number <= 452) {
      return "Triphosphate";
    } else if(number >= 459 && number <= 459) {
      return "Cyclodextrine";
    } else if(number >= 460 && number <= 469) {
      return "Dérivé cellulosique";
    } else if(number >= 470 && number <= 495) {
      return "Dérivé d'acides gras alimentaires";
    } else if(number >= 500 && number <= 504) {
      return "Carbonate";
    } else if(number >= 507 && number <= 512) {
      return "Chlorure";
    } else if(number >= 513 && number <= 523) {
      return "Sulfate";
    } else if(number >= 524 && number <= 530) {
      return "Hydroxyde";
    } else if(number >= 535 && number <= 538) {
      return "Ferrocyanure";
    } else if(number >= 541 && number <= 541) {
      return "Phosphate";
    } else if(number >= 551 && number <= 560) {
      return "Silicate";
    } else if(number >= 570 && number <= 570) {
      return "Stéarate";
    } else if(number >= 574 && number <= 586) {
      return "Gluconate";
    } else if(number >= 620 && number <= 625) {
      return "Exhausteur de goût - Glutamate";
    } else if(number >= 626 && number <= 629) {
      return "Guanylate";
    } else if(number >= 630 && number <= 633) {
      return "Inosinate";
    } else if(number >= 634 && number <= 650) {
      return "Diver";
    } else if(number >= 900 && number <= 903) {
      return "Cire et hydrocarbure";
    } else if(number >= 904 && number <= 904) {
      return "Gomme à mâcher";
    } else if(number >= 905 && number <= 927) {
      return "Glace";
    } else if(number >= 938 && number <= 949) {
      return "Gaz propulseur, inerteur, conditionneur ou traceur";
    } else if(number >= 950 && number <= 968) {
      return "Édulcorant";
    } else if(number >= 999 && number <= 1205) {
      return "Diver";
    } else if(number >= 1404 && number <= 1452) {
      return "Amidon modifié";
    } else if(number >= 1500 && number <= 1521) {
      return "Diver";
    } else {
      return "Non répertorié";
    }
  }

  static String getAdditiveLink(String additive) {
    int number = int.parse(additive.replaceAll(RegExp(r'[^0-9]+'),''));

    return "https://fr.wikipedia.org/wiki/E$number";
  }

}