
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wimplate/core/apiModels.dart';
import 'package:wimplate/core/userdataController.dart';
import 'package:wimplate/elements/compareDialogBody.dart';
import 'package:wimplate/elements/wimplateDialog.dart';

import '../createListPage.dart';

class WimplateActionsController {

  static void takeProduct(OpenFoodFact product, BuildContext context, GlobalKey<ScaffoldState> _scaffoldKey, Function reloadPrefs) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var novaScore = product.getNovaGroup() * (-1) + 2;
    var additivesScore = product.getAdditives().length;
    var organicScore = product.isOrganic() ? 1 : 0;

    var total = product.getNutritionScore() - additivesScore + organicScore;

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return WimplateDialog(
          Container(
              height: MediaQuery.of(context).size.height / 2,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text("Mon coach santé", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),),
                          ),
                        ],
                      ),
                      Container(height: 10.0,),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text("Consommer ce produit va influencer votre compteur de Wims", style: TextStyle(color: Colors.grey, fontSize: 14.0, fontStyle: FontStyle.italic),),
                          ),
                        ],
                      ),
                      Container(height: 20.0,),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text("Résumé", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.bold),),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text("Nutrition", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal),),
                            ),
                            Flexible(
                              child: Text("${product.getNutritionScore() > 0 ? '+' : ''}${product.getNutritionScore()}", style: TextStyle(color: product.getNutritionScore() > 0 ? Colors.greenAccent : Colors.redAccent, fontSize: 14.0, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text("Produit transformé (NOVA)", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal),),
                            ),
                            Flexible(
                              child: Text("${novaScore >= 0 ? '+' : ''}$novaScore", style: TextStyle(color: novaScore >= 0 ? Colors.greenAccent : Colors.redAccent, fontSize: 14.0, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text("Additifs", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal),),
                            ),
                            Flexible(
                              child: Text("-$additivesScore", style: TextStyle(color: Colors.redAccent, fontSize: 14.0, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text("Produit biologique", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal),),
                            ),
                            Flexible(
                              child: Text("+$organicScore", style: TextStyle(color: Colors.greenAccent, fontSize: 14.0, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 1.0,
                        color: Colors.grey,
                        margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 8.0, bottom: 8.0),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text("Total", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal),),
                            ),
                            Flexible(
                              child: Text("${total > 0 ? '+' : ''}$total", style: TextStyle(color: total > 0 ? Colors.greenAccent : Colors.redAccent, fontSize: 14.0, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 40.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        MaterialButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("ANNULER", style: TextStyle(color: Colors.white),),
                        ),
                        MaterialButton(
                          onPressed: () async {
                            int wimsCount = prefs.getInt('wims') ?? 100;

                            wimsCount += total;

                            print(wimsCount);

                            UserdataController.addProductToList(product, "list2");

                            await prefs.setInt('wims', wimsCount);
                            reloadPrefs();

                            Navigator.of(context).pop();

                            _scaffoldKey.currentState.showSnackBar(
                              SnackBar(
                                content: Text("Votre compteur Wims a été mis à jour", style: TextStyle(color: Colors.white),),
                                action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
                                backgroundColor: Colors.black.withOpacity(0.8),
                              ),
                            );
                          },
                          child: Text("OK", style: TextStyle(color: Colors.white),),
                        ),
                      ],
                    ),
                  ),
                ],
              )
          ),
        );
      },
    );
  }

  static void addToList(BuildContext context, Function reloadPrefs, OpenFoodFact product, GlobalKey<ScaffoldState> _scaffoldKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var myLists = prefs.getString("myLists").split(';');
    myLists.removeAt(2);
    myLists.removeAt(0);

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WimplateDialog(
              Container(
                height: MediaQuery.of(context).size.height / 2,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Flexible(
                          child: Text("Mes listes", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),),
                        ),
                      ],
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: ListView.builder(
                          itemCount: myLists.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: EdgeInsets.symmetric(vertical: 5.0),
                              child: InkWell(
                                onTap: () {
                                  UserdataController.addProductToList(product, myLists[index].split(',')[1]);
                                  reloadPrefs();
                                  Navigator.of(context).pop();
                                  _scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Le produit a bien été ajouté à ${myLists[index].split(',')[0]}", style: TextStyle(color: Colors.white),),
                                      action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
                                      backgroundColor: Colors.black.withOpacity(0.8),
                                    ),
                                  );
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 50.0,
                                      height: 50.0,
                                      margin: EdgeInsets.only(right: 10.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: NetworkImage(myLists[index].split(',')[2]),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Text(myLists[index].split(',')[0], style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.bold),),
                                  ],
                                ),
                              ),
                            );
                          }
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              MaterialButton(
                                onPressed: () async {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => CreateListPage(addToList)),);
                                },
                                child: Text("NOUVELLE LISTE", style: TextStyle(color: Colors.white),),
                              ),
                              MaterialButton(
                                onPressed: () async {
                                  Navigator.of(context).pop();
                                },
                                child: Text("ANNULER", style: TextStyle(color: Colors.white),),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
          );
        }
    );
  }

  static void compare(OpenFoodFact product, BuildContext context, GlobalKey<ScaffoldState> _scaffoldKey, Function reloadPrefs) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var myProducts = prefs.getString("list0").split(';').reversed.toList();

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          var widget;
          return WimplateDialog(
            CompareDialogBody(myProducts, product, _scaffoldKey, context, reloadPrefs),
          );
        }
    );
  }

}