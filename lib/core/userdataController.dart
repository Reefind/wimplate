
import 'package:shared_preferences/shared_preferences.dart';

import 'apiModels.dart';

class UserdataController {

  Future<SharedPreferences> getSharedPrefsInstance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  static Future<List<String>> getLists() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String lists = prefs.getString("myLists");

    return lists.split(';');
  }

  static void addProductToList(OpenFoodFact product, String listId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String list = prefs.getString(listId);

    if(list.contains(product.getBarcode())) return;

    String newProduct = "${product.getName().replaceAll(',', '')},${product.getBarcode()},${product.getImageUrl()},${product.getGenericName().replaceAll(',', '')},${product.getBrand().replaceAll(',', '')}, ${product.getNutritionGrade()}";

    if(list == "") {
      list = newProduct;
    } else {
      list += ";$newProduct";
    }

    prefs.setString(listId, list);
  }

  static void createList(String name, String illustration) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String lists = prefs.getString("myLists");

    String listId = "list" + ';'.allMatches(lists).length.toString();
    name = name.replaceAll(';', ' ');

    if(lists == "") {
      lists = "$name,$listId,$illustration";
    } else {
      lists += ";$name,$listId,$illustration";
    }

    prefs.setString("myLists", lists);
  }

  static void removeList(String listId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> lists = prefs.getString("myLists").split(';');

    lists.asMap().forEach((i, list) {
      var props = list.split(',');
      if(props[1] == listId) {
        lists.removeAt(i);
      }
    });

    String result = lists.join(',');
    prefs.setString(listId, result);

    prefs.setString(listId, null);
  }

  static void removeProductFromList(String listId, String productBarcode) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> list = prefs.getString(listId).split(';');

    var index = -1;

    list.asMap().forEach((i, product) {
      var props = product.split(',');
      if(props[1] == productBarcode) {
        index = i;
        return;
      }
    });

    if(index != -1) {
      list.removeAt(index);
    }

    String result = list.join(';');

    prefs.setString(listId, result);
  }

}