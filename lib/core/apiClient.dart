
import 'dart:convert';
import 'dart:io';

import 'package:wimplate/core/apiModels.dart';

class APIClient {

  static String openFoodFactUrl = "https://fr.openfoodfacts.org/api/v0/produit/";

  static Future<OpenFoodFact> getFoodFact(String code) async {
    String url = openFoodFactUrl + code + ".json";

    print("Fetching : $code");

    var data = await _fetch(url);

    //print(data.runtimeType);

    return OpenFoodFact(data);
  }

  static dynamic _fetch(String url) async {

    HttpClient client = HttpClient();

    HttpClientRequest request;

    try {
      request = await client.getUrl(Uri.parse(url));
      request.headers.set("UserAgent", "Wimplate - Android - Version 0.0.1 - URL coming soon");
    } catch(error) {
      print(error);
      return null;
    }

    HttpClientResponse response = await request.close();

    if(response.statusCode == 200) {
      String json = await response.transform(utf8.decoder).join();
      if(json == "null") {
        print("Received null");
        return null;
      }
      print("Success");
      return jsonDecode(json);
    } else {
      print("Failed to retreive Http request : ${response.statusCode}");
      return null;
    }
  }


}