
import 'dataHolder.dart';

class OpenFoodFact {

  var data;

  OpenFoodFact(this.data);

  int getStatus() {
    return data["status"];
  }

  String getBarcode() {
    return data["product"]["code"];
  }

  String getName() {
    var value = data["product"]["product_name_fr"];

    if(value == null) {
      value = data["product"]["product_name"];
    }

    return value;
  }

  String getGenericName() {
    var value = data["product"]["generic_name_fr"];

    if(value == null) {
      value = data["product"]["generic_name"];
    }

    return value;
  }

  String getImageUrl() {
    return data["product"]["image_front_small_url"];
  }

  String getBrand() {
    return data["product"]["brands"].toString().split(',')[0];
  }

  int getScore() {
    var score = data["product"]["nutriments"]["nutrition-score-fr_100g"];

    if(score == null) {
      score = data["product"]["nutriments"]["nutrition-score-fr"];
      if(score == null) {
        score = data["product"]["nutriments"]["nutrition-score-uk_100g"];
      }
    }

    return score;
  }

  int getNovaGroup() {
    var group = data["product"]["nova_group"];

    if(group == null) {
      group = data["product"]["nova_groups"];
    }

    if(group != null) {
      group = int.parse(group.toString());
    }

    return group;
  }

  String getNutritionGrade() {
    var grade = data["product"]["nutrition_grade_fr"];

    if(grade == null) {
      grade = data["product"]["nutrition_grade"];
    }

    return grade;
  }

  int getNutritionScore() {
    switch(getNutritionGrade()) {
      case "a": return 2; break;
      case "b": return 1; break;
      case "c": return 0; break;
      case "d": return -1; break;
      case "e": return -2; break;
      default: return 0;
    }
  }

  bool isOrganic() {
    var labels = data["product"]["labels_tags"];

    if(labels != null) {
      for(var label in labels) {
        if(label.toString().contains("en:organic") || label.toString().contains("en:eu-organic") || label.toString().contains("fr:ab-agriculture-biologique")) {
          return true;
        }
      }
    } else {
      labels = data["product"]["labels_prev_tags"];

      if(labels != null) {
        for(var label in labels) {
          if(label.toString().contains("en:organic") || label.toString().contains("en:eu-organic") || label.toString().contains("fr:ab-agriculture-biologique")) {
            return true;
          }
        }
      } else {
        labels = data["product"]["labels_hierarchy"];

        if(labels != null) {
          for(var label in labels) {
            if(label.toString().contains("en:organic") || label.toString().contains("en:eu-organic") || label.toString().contains("fr:ab-agriculture-biologique")) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  List<String> getAdditives() {
    var value = data["product"]["additives_tags"];

    if(value == null) {
      value = data["product"]["additives_old_tags"];
    }

    List<String> result = List(value.length);

    for(var i = 0; i < value.length; i++) {
      result[i] = value[i].substring(3).toString();
    }

    return result;
  }

  String getSaturatedFatScore() {
    var value = data["product"]["nutrient_levels"]["saturated-fat"];

    if(value == null) {
      return "";
    }

    return value;
  }

  String getFatScore() {
    var value = data["product"]["nutrient_levels"]["fat"];

    if(value == null) {
      return "";
    }

    return value;
  }

  String getSugarScore() {
    var value = data["product"]["nutrient_levels"]["sugars"];

    if(value == null) {
      return "";
    }

    return value;
  }

  String getSaltScore() {
    var value = data["product"]["nutrient_levels"]["salt"];

    if(value == null) {
      return "";
    }

    return value;
  }

  String getNutrimentsEnergy() {
    var energy = data["product"]["nutriments"]["energy_value"];

    if(getEnergyUnit() == "kJ" || getEnergyUnit() == "kj" || getEnergyUnit() == "KJ") {
      energy = (energy * 0.2388).round();
    }

    return energy.toString();
  }

  String getGroup() {
    return data["product"]["pnns_groups_2"];
  }

  String getIllustration() {
    return DataHolder.groupIllustrations[getGroup()];
  }

  String getNutrimentsSaturatedFats() {
    var value = data["product"]["nutriments"]["saturated-fat_value"].toString();

    if(value == null) {
      return "";
    }

    return value;
  }

  String getNutrimentsCarbohydrates() {
    var value = data["product"]["nutriments"]["carbohydrates_value"].toString();
    if(value == null) {
      value = data["product"]["nutriments"]["carbohydrates_100g"].toString();
    }
    return value;
  }

  String getNutrimentsSugar() {
    var value = data["product"]["nutriments"]["sugars"].toString();
    if(value == null) {
      value = data["product"]["nutriments"]["sugars_100g"].toString();
      if(value == null) {
        value = data["product"]["nutriments"]["sugars_value"].toString();
      }
    }
    return value;
  }

  String getNutrimentsFat() {
    var value = data["product"]["nutriments"]["fat_100g"].toString();

    if(value == null) {
      value = data["product"]["nutriments"]["fat_value"].toString();
    }

    return value;
  }

  String getNutrimentsSalt() {
    return data["product"]["nutriments"]["salt_100g"].toString();
  }

  String getNutrimentsFiber() {
    var value = data["product"]["nutriments"]["fiber"];
    if(value == null) {
      value = "0";
    }
    return value.toString();
  }

  String getNutrimentsProteins() {
    var value = data["product"]["nutriments"]["proteins"].toString();

    if(value == null) {
      value = data["product"]["nutriments"]["proteins_value"].toString();
    }

    return value;
  }

  String getIngredients() {
    var value = data["product"]["ingredients_text"].toString(); //data["product"]["ingredients_text_with_allergens_fr"].toString();

    if(value == null) {
      value =  data["product"]["ingredients_text"].toString();
    }

    return value;
  }

  List<Ingredient> getOrderedIngredients() {
    var ingredients =  data["product"]["ingredients"];

    var result = List<Ingredient>();

    int index = 0;
    bool reachedEnd = false;

    while(!reachedEnd) {
      var tempIngredient;
      try {
        tempIngredient = ingredients[index];
      } catch(error) {
        reachedEnd = true;
        break;
      }

      var text = tempIngredient["text"];
      var percentage = tempIngredient["percentage"];

      var ingredient = Ingredient(text, percentage);

      result.add(ingredient);

      index++;
    }

    return result;
  }

  String getEnergyUnit() {
    return data["product"]["nutriments"]["energy_unit"].toString();
  }

}

class Ingredient {

  String text;
  var percentage;

  Ingredient(this.text, this.percentage);

  String getText() {
    return this.text;
  }

  String getPercentage() {
    return this.percentage.toString();
  }
}