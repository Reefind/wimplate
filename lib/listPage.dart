
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wimplate/core/apiClient.dart';
import 'package:wimplate/core/userdataController.dart';
import 'package:wimplate/elements/wimplateDialog.dart';

import 'productPage.dart';

class ListPage extends StatefulWidget {

  final String listName;
  final String listId;
  final String listIllustration;
  final Function reloadPrefs;

  ListPage(this.listName, this.listId, this.listIllustration, this.reloadPrefs);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {

  List<String> products;
  bool listLoaded = false;

  @override
  void initState() {
    super.initState();

    loadList();
  }

  void loadList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String productsString = prefs.getString(widget.listId);

    products = [];

    if(productsString != "") {
      products = productsString.split(';');
    }

    setState(() {
      listLoaded = true;
    });
  }

  Future open(String barcode) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WimplateDialog(
          Container(
            height: 100.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );

    var foodFact = await APIClient.getFoodFact(barcode);
    Navigator.of(context).pop();
    Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPage(foodFact, barcode, widget.reloadPrefs)),);
  }

  int getIndex(String grade) {
    switch(grade.replaceAll(' ', '')) {
      case "a": return 0; break;
      case "b": return 1; break;
      case "c": return 2; break;
      case "d": return 3; break;
      case "e": return 4; break;
      default: return 5;
    }
  }

  List<Color> colors = [Colors.green, Colors.greenAccent, Colors.orangeAccent, Colors.deepOrangeAccent, Colors.redAccent, Colors.white];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0f0f0f),
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Hero(
                      tag: widget.listId + "_herotag",
                      child: Container(
                        width: 120.0,
                        height: 120.0,
                        margin: EdgeInsets.only(bottom: 15.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(65.0)),
                          color: Colors.grey,
                          border: Border.all(color: Colors.white, width: 3.0),
                          image: DecorationImage(image: NetworkImage(widget.listIllustration), fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            child: Text(widget.listName, style: TextStyle(color: Colors.white, fontSize: 26.0), textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          listLoaded ? products.length == 0 ? Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.25),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text("Aucun produit ajouté à cette liste", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic, fontSize: 18.0),),
                ),
              ],
            ),
          ) : Expanded(
            child: ListView.builder(
              itemCount: products.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return Dismissible(
                  key: Key(products[index]),
                  onDismissed: (direction) {

                    UserdataController.removeProductFromList(widget.listId, products[index].split(',')[1]);

                    setState(() {
                      products.removeAt(index);
                    });

                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text("Produit supprimé", style: TextStyle(color: Colors.white),),
                        action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
                        backgroundColor: Colors.black.withOpacity(0.8),
                      ),
                    );
                  },
                  child: GestureDetector(
                    onTap: () async {
                      showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return WimplateDialog(
                            Container(
                              height: 100.0,
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            ),
                          );
                        },
                      );
                      var barcode = products[index].split(',')[1];
                      var foodFact = await APIClient.getFoodFact(barcode);
                      Navigator.of(context).pop();
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPage(foodFact, barcode, widget.reloadPrefs)),);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        color: Colors.white12,
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: 80.0,
                                      height: 80.0,
                                      margin: EdgeInsets.all(15.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: NetworkImage(products[index].split(',')[2]),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.45,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            products[index].split(',')[0],
                                            style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          Text(
                                            products[index].split(',')[3],
                                            style: TextStyle(color: Colors.white, fontSize: 18.0, fontStyle: FontStyle.italic),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          Text(
                                            products[index].split(',')[4],
                                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Center(
                                      child: Container(
                                        margin: EdgeInsets.symmetric(horizontal: 15.0),
                                        width: 30.0,
                                        height: 30.0,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: colors[getIndex(products[index].split(',')[5])],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(

                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }
            ),
          ) : Container(),
        ],
      ),
    );
  }

}