import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:qr_utils/qr_utils.dart';
import 'package:rubber/rubber.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:wimplate/core/apiClient.dart';
import 'package:wimplate/productPage.dart';

import 'core/userdataController.dart';
import 'package:wimplate/listPage.dart';
import 'createListPage.dart';
import 'elements/wimplateDialog.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'Wimplate',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Wimplate'),
    );
  }
}

class MyHomePage extends StatefulWidget{
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  RubberAnimationController _rubberController;

  var stateColor = Colors.greenAccent;
  var scanCount = 0;
  var addCount = 0;
  var wimsCount = 0;
  var myLists = List<String>();
  var prefsLoaded = false;

  Future scan() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WimplateDialog(
          Container(
            height: 100.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );

    try {
      final barcode = await QrUtils.scanQR;
      print(barcode);

      if(barcode != null) {
        var foodFact = await APIClient.getFoodFact(barcode);

        Navigator.of(context).pop();

        if(foodFact.getStatus() == 0) {
          _scaffoldKey.currentState.showSnackBar(
              SnackBar(
                content: Text("Aucun produit ne correspond au code bar", style: TextStyle(color: Colors.redAccent),),
                action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
                backgroundColor: Colors.black.withOpacity(0.8),
              )
          );
          print("Produit non trouvé");
        } else {
          /*_scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text("Produit trouvé", style: TextStyle(color: Colors.redAccent),),
            )
          );*/

          SharedPreferences prefs = await SharedPreferences.getInstance();

          getPrefs();

          print("Produit trouvé");
          var history = prefs.getString("list0");
          if(!history.contains(barcode)) {
            UserdataController.addProductToList(foodFact, "list0");
          }
          Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPage(foodFact, barcode, getPrefs)),);
          //print("Nav passed");
        }
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text("Aucun code bar détecté", style: TextStyle(color: Colors.redAccent),),
            action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
            backgroundColor: Colors.black.withOpacity(0.8),
          ),
        );
      }
    } on PlatformException catch (e) {
      print(e);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Erreur : $e", style: TextStyle(color: Colors.redAccent),),
        ),
      );
      print(e);
    }
  }

  Future open(String barcode) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WimplateDialog(
          Container(
            height: 100.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );

    var foodFact = await APIClient.getFoodFact(barcode);
    Navigator.of(context).pop();
    Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPage(foodFact, barcode, getPrefs)),);
  }

  @override
  void initState() {
    _rubberController = RubberAnimationController(
        vsync: this,
        halfBoundValue: AnimationControllerValue(percentage: 0.5),
        lowerBoundValue: AnimationControllerValue(pixel: 100),
        duration: Duration(milliseconds: 200),
    );
    _rubberController.addStatusListener(_rubberStatusListener);
    _rubberController.animationState.addListener(_rubberStateListener);

    checkFirstLaunch();

    super.initState();
  }

  void checkFirstLaunch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    //prefs.setBool('firstLaunch', true);

    bool isFirstLaunch = prefs.getBool('firstLaunch') ?? true;

    if(isFirstLaunch) {
      prefs.setString('myLists', "Historique,list0,https://images.unsplash.com/photo-1490645935967-10de6ba17061?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500;Ma liste,list1,https://images.unsplash.com/photo-1490474418585-ba9bad8fd0ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500;Produits consommés,list2,https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500");
      prefs.setString('list0', "");
      prefs.setString('list1', "");
      prefs.setString('list2', "");
      prefs.setInt('wims', 500);
      prefs.setBool('firstLaunch', false);

      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context) {
            return WimplateDialog(
                Container(
                  height: MediaQuery.of(context).size.height * 0.6,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.5,
                        child: ListView(
                          children: <Widget>[
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  child: Text("Bienvenue", style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold),),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.all(15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    child: Text("Wimplate est une application open source et 100% transparente.", style: TextStyle(color: Colors.grey, fontSize: 14.0, fontStyle: FontStyle.italic),),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(right: 10.0, top: 10.0, bottom: 10.0),
                                  child: Icon(
                                    Icons.portable_wifi_off,
                                    color: Colors.greenAccent,
                                    size: 30.0,
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    "Aucun serveur : vos données sont les vôtres et restent à tout moment entre vos mains.",
                                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                            Container(height: 5.0,),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(right: 10.0, top: 10.0, bottom: 10.0),
                                  child: Icon(
                                    Icons.people,
                                    color: Colors.greenAccent,
                                    size: 30.0,
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    "Communautaire : Wimplate utilise la base de données OpenFoodFact et vous permettra prochainement d'y participer.",
                                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                            Container(height: 5.0,),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(right: 10.0, top: 10.0, bottom: 10.0),
                                  child: Icon(
                                    Icons.money_off,
                                    color: Colors.greenAccent,
                                    size: 30.0,
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    "Gratuit : toute les fonctionnalités sont gratuites et le resteront toujours.",
                                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                            Container(height: 5.0,),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(right: 10.0, top: 10.0, bottom: 10.0),
                                  child: Icon(
                                    Icons.warning,
                                    color: Colors.redAccent,
                                    size: 30.0,
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    "Attention : les données d'OpenFoodFact sont en constante amélioration, les informations comme les allergènes peuvent être non vérifiées.",
                                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                MaterialButton(
                                  onPressed: () async {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("SUPER !", style: TextStyle(color: Colors.white),),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
            );
          }
      );
    }

    getPrefs();
  }

  void getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String history = prefs.getString('list0');
    String added = prefs.getString('list2');

    setState(() {
      wimsCount = prefs.getInt('wims') ?? 500;

      if(wimsCount < 200) {
        stateColor = Colors.redAccent;
      } else if(wimsCount < 400) {
        stateColor = Colors.orangeAccent;
      } else if(wimsCount < 600) {
        stateColor = Colors.greenAccent;
      } else {
        stateColor = Colors.lightBlueAccent;
      }

      scanCount = history == "" ? 0 : history.split(';').length;
      myLists = prefs.getString('myLists').split(';');
      print("Lists : " + myLists[0].split(',')[1]);
      addCount = added == "" ? 0 : added.split(';').length;
      prefsLoaded = true;
    });
  }

  @override
  void dispose() {
    _rubberController.animationState.removeListener(_rubberStateListener);
    _rubberController.removeStatusListener(_rubberStatusListener);
    super.dispose();
  }

  void _rubberStateListener() {
    print("state changed ${_rubberController.animationState.value}");
  }

  void _rubberStatusListener(AnimationStatus status) {
    print("changed status ${_rubberController.status}");
  }

  void _rubberExpand() {
    _rubberController.expand();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/home_background.jpeg"),
            fit: BoxFit.fitHeight,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.hardLight),
          ),
        ),
        child: RubberBottomSheet(
          lowerLayer: _getLowerLayer(),
          upperLayer: _getUpperLayer(),
          animationController: _rubberController,
        ),
      ),
    );
  }

  Widget _getLowerLayer() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 120.0),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Wimplate", style: TextStyle(color: Colors.white, fontSize: 42.0, fontWeight: FontWeight.bold, fontFamily: 'Pacifico'), textAlign: TextAlign.start,),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Pour commencer veuillez scanner un produit", style: TextStyle(color: Colors.white, fontSize: 16.0, fontStyle: FontStyle.italic), textAlign: TextAlign.start,),
              ],
            ),
          ),
          GestureDetector(
            onTap: scan,
            child: Container(
              margin: EdgeInsets.only(top: 150.0),
              width: 100.0,
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(100.0)),
                border: Border.all(color: Colors.white, width: 2.0),
              ),
              child: Icon(CupertinoIcons.photo_camera, color: Colors.white, size: 60.0,),
            ),
          ),
        ],
      ),
    );
  }
  Widget _getUpperLayer() {
    return ClipRRect(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
        child: BackdropFilter(
          filter: ImageFilter.blur(
          sigmaX: 2.0,
          sigmaY: 2.0,
        ),
        child: Container(
          padding: EdgeInsets.only(top: 0.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
            color: Color(0xFF1f2021).withOpacity(0.95),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: ListView(
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    GestureDetector(
                      onTap: _rubberExpand,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100.0,
                            height: 4.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(4.0)),
                              color: Colors.white.withOpacity(0.2),
                            ),
                            margin: EdgeInsets.only(top: 0.0, bottom: 20.0),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: 30.0,
                              height: 30.0,
                              margin: EdgeInsets.only(left: 30.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                color: Colors.greenAccent,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 20.0),
                              padding: EdgeInsets.all(5.0),
                              child: Row(
                                children: <Widget>[
                                  Text("Mon Wimplate", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontFamily: 'Pacifico', fontSize: 18.0),),
                                ],
                              ),
                            ),
                          ],
                        ),
                        prefsLoaded ? Container(
                          margin: EdgeInsets.only(right: 25.0),
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            children: <Widget>[
                              Text("${wimsCount.toString()} Wims", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 14.0),),
                            ],
                          ),
                        ) : Container(),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            children: <Widget>[
                              Text("Produits scannés", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 18.0),),
                              Container(height: 15.0,),
                              Text(scanCount.toString(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 28.0),),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            children: <Widget>[
                              Text("Produits Consommés", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 18.0),),
                              Container(height: 15.0,),
                              Text(addCount.toString(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 28.0),),
                            ],
                          ),
                        ),
                      ],
                    ),

                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 40.0, bottom: 0.0, left: 20.0),
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Text("Mon coach santé :", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0),),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(MediaQuery.of(context).size.width * ((wimsCount - 400) / 1000.0), 0.0), // Value between -0.4 & 0.4
                              child: Column(
                                children: <Widget>[
                                  Transform.translate(
                                    offset: Offset(0.0, 15.0),
                                    child: Container(
                                      child: Text(wimsCount.toString(), style: TextStyle(color: Colors.white),),
                                    ),
                                  ),
                                  Container(
                                    width: 50.0,
                                    height: 35.0,
                                    child: Icon(Icons.arrow_drop_down, color: Colors.white, size: 50.0,),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            /*Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 2.0,
                                  height: 25.0,
                                  color: Colors.white,
                                ),
                                Text(
                                  '0',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),*/
                            Container(
                              width: MediaQuery.of(context).size.width * 0.1,
                              child: Text(
                                '0',
                                style: TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              height: 10.0,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [Colors.redAccent, Colors.orangeAccent, Colors.greenAccent, Colors.lightBlueAccent]),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.1,
                              child: Text(
                                '800',
                                style: TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            /*Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                /*Container(
                                  width: 2.0,
                                  height: 25.0,
                                  color: Colors.white,
                                ),*/
                                Text(
                                  '800',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),*/
                          ],
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40.0, bottom: 25.0, left: 20.0),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: Text("Mes Listes :", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0),),
                          ),
                        ],
                      ),
                    ),
                    (prefsLoaded || myLists.length != 0) ? Container(
                      height: 150.0,
                      child: ListView.builder(
                            itemCount: myLists.length + 1,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              if(index == myLists.length) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => CreateListPage(getPrefs)),
                                    );
                                  },
                                  child: Center(
                                    child: Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20.0),
                                      width: 80.0,
                                      height: 80.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(color: Colors.white, width: 2.0)
                                      ),
                                      child: Center(
                                        child: Icon(Icons.add, color: Colors.white, size: 35.0,),
                                      ),
                                    ),
                                  ),
                                );
                              }

                              return GestureDetector(
                                onTap: () {
                                  //var barcode = myLists[index].split(',')[0];
                                  //open(barcode);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => ListPage(myLists[index].split(',')[0], myLists[index].split(',')[1], myLists[index].split(',')[2], getPrefs)),
                                  );
                                },
                                child: Container(
                                  width: 220.0,
                                  height: 150.0,
                                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                                      color: Colors.grey,
                                      image: DecorationImage(image: NetworkImage(myLists[index].split(',')[2]), fit: BoxFit.cover),
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Hero(
                                            tag: myLists[index].split(',')[1] + "_herotag",
                                            child: Container(
                                              width: 65.0,
                                              height: 65.0,
                                              margin: EdgeInsets.only(bottom: 15.0, left: 10.0),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(65.0)),
                                                color: Colors.grey,
                                                border: Border.all(color: Colors.white, width: 3.0),
                                                image: DecorationImage(image: NetworkImage(myLists[index].split(',')[2]), fit: BoxFit.cover),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        width: 220.0,
                                        height: 60.0,
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20.0), bottomRight: Radius.circular(20.0)),
                                            color: Colors.black.withOpacity(0.6),
                                        ),
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Flexible(
                                                  child: Text(myLists[index].split(',')[0], style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold), maxLines: 2, overflow: TextOverflow.ellipsis,),
                                                ),
                                              ],
                                            ),
                                            /*Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Flexible(
                                                  child: Text(myLists[index].split(',')[2], style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic), maxLines: 1, overflow: TextOverflow.ellipsis,),
                                                ),
                                              ],
                                            ),*/
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }
                        ),
                    ) : Container(
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: Text("Scannez des produits pour les ajouter ici", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic, fontSize: 16.0),),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
