

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/dataHolder.dart';

class CreateListPage extends StatefulWidget {

  final Function reloadLists;

  CreateListPage(this.reloadLists);

  @override
  _CreateListPageState createState() => _CreateListPageState();
}

class _CreateListPageState extends State<CreateListPage> {

  var selectedIndex;

  final nameController = TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void createList() async {
    String name = nameController.text;

    if(name == "") {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Veuillez renseigner un nom", style: TextStyle(color: Colors.white),),
          action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
          backgroundColor: Colors.black.withOpacity(0.8),
        ),
      );
    }

    if(selectedIndex == null) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Veuillez selectionner une illustration", style: TextStyle(color: Colors.white),),
          action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
          backgroundColor: Colors.black.withOpacity(0.8),
        ),
      );
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var myLists = prefs.getString("myLists");

    String newId = "list${myLists.split(';').length}";

    myLists += ";$name,$newId,${DataHolder.illustrations.values.toList()[selectedIndex]}";

    prefs.setString("myLists", myLists);
    prefs.setString(newId, "");

    Navigator.of(context).pop();
    Navigator.of(context).pop();
    widget.reloadLists();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xFF0f0f0f),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: createList,
        child: Icon(Icons.check, color: Colors.black,),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextFormField(
              controller: nameController,
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                labelText: 'Nom',
                labelStyle: TextStyle(color: Colors.white),
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white ,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white ,
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Text(
                    "Choisissez une illustration",
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 500.0,
            child: GridView.count(
              crossAxisCount: 2,
              childAspectRatio: 5.0 / 3.5,
              children: List.generate(DataHolder.illustrations.keys.length, (index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedIndex = index;
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      color: Colors.white12,
                      image: DecorationImage(image: NetworkImage(DataHolder.illustrations.values.toList()[index]), fit: BoxFit.cover),
                    ),
                    child: selectedIndex == index ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        border: Border.all(color: Colors.white, width: 2.0),
                      ),
                      child: Center(
                        child: Container(
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white30,
                            border: Border.all(color: Colors.white, width: 2.0),
                          ),
                          child: Center(
                            child: Icon(Icons.check, color: Colors.greenAccent,),
                          ),
                        ),
                      ),
                    ) : Container(),
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }

}