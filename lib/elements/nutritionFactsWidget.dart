
import 'package:flutter/material.dart';
import 'package:wimplate/core/apiModels.dart';

class NutritionFactsWidget extends StatelessWidget {

  final OpenFoodFact product;

  NutritionFactsWidget(this.product);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Énergie", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsEnergy()} kCal / 2000 kCal", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Matières grasses", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsFat()} g / 70 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("    dont Acides gras saturés", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsSaturatedFats()} g / 20 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Glucides", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsCarbohydrates()} g / 260 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("    dont Sucres", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsSugar()} g / 90 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Sel", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsSalt()} g / 6 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Fibres", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsFiber()} g / 30 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 15.0, right: 20.0),
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Protéines", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              Text("${product.getNutrimentsProteins()} g / 50 g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
            ],
          ),
        ),
      ],
    );
  }

}