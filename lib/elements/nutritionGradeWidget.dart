
import 'package:flutter/material.dart';

class NutritionGradeWidget extends StatelessWidget {

  final String grade;
  final double width;

  NutritionGradeWidget(this.grade, this.width);

  TextStyle inactive = TextStyle(fontSize: 28.0, color: Colors.white.withOpacity(0.5), fontStyle: FontStyle.normal, fontWeight: FontWeight.bold);

  int getIndex() {
    switch(this.grade) {
      case "a": return 0; break;
      case "b": return 1; break;
      case "c": return 2; break;
      case "d": return 3; break;
      case "e": return 4; break;
      default: return 5;
    }
  }

  List<Color> colors = [Colors.green, Colors.greenAccent, Colors.orangeAccent, Colors.deepOrangeAccent, Colors.redAccent];

  @override
  Widget build(BuildContext context) {
    return getIndex() != 5 ? Container(
      width: this.width + 20.0,
      height: 80.0,
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10.0, top: 10.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  width: this.width / 5,
                  height: 60.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20.0), topLeft: Radius.circular(20.0)),
                    color: Colors.green,
                  ),
                  child: Center(
                    child: Text("A", style: inactive),
                  ),
                ),
                Container(
                  width: this.width / 5,
                  height: 60.0,
                  color: Colors.greenAccent,
                  child: Center(
                    child: Text("B", style: inactive),
                  ),
                ),
                Container(
                  width: this.width / 5,
                  height: 60.0,
                  color: Colors.orangeAccent,
                  child: Center(
                    child: Text("C", style: inactive),
                  ),
                ),
                Container(
                  width: this.width / 5,
                  height: 60.0,
                  color: Colors.deepOrangeAccent,
                  child: Center(
                    child: Text("D", style: inactive),
                  ),
                ),
                Container(
                  width: this.width / 5,
                  height: 60.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomRight: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                    color: Colors.redAccent,
                  ),
                  child: Center(
                    child: Text("E", style: inactive),
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(5 + 2.5 * getIndex() + (this.width / 5) * getIndex(), 0.0),
            child: Container(
              width: this.width / 5 + 10.0,
              height: 80.0,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 4.0),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                color: colors[getIndex()],
              ),
              child: Center(
                child: Text(this.grade.toUpperCase(), style: TextStyle(color: Colors.white, fontSize: 34.0, fontWeight: FontWeight.bold),),
              ),
            ),
          ),

        ],
      )
    ) : Container(
      width: this.width + 20.0,
      height: 60.0,
      child: Center(
        child: Text("Nutri-score introuvable", style: TextStyle(color: Colors.grey, fontSize: 14.0, fontStyle: FontStyle.italic),),
      ),
    );
  }

}