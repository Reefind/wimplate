
import 'dart:ui';

import 'package:flutter/material.dart';

class WimplateDialog extends StatelessWidget {

  final Widget content;

  WimplateDialog(this.content);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 2.0,
          sigmaY: 2.0,
        ),
        child: Container(
          padding: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            color: Color(0xFF1f2021).withOpacity(0.95),
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: this.content,
        ),
      )
    );
  }

}