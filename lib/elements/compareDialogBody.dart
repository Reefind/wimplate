
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:qr_utils/qr_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:wimplate/core/apiClient.dart';
import 'package:wimplate/core/apiModels.dart';
import 'package:wimplate/core/userdataController.dart';
import 'package:wimplate/elements/wimplateDialog.dart';

import '../comparePage.dart';

class CompareDialogBody extends StatelessWidget {

  final List<String> myProducts;
  final OpenFoodFact product;
  final GlobalKey<ScaffoldState> _scaffoldKey;
  final BuildContext context;
  final Function reloadPrefs;

  CompareDialogBody(this.myProducts, this.product, this._scaffoldKey, this.context, this.reloadPrefs);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 2,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Flexible(
                child: Text("Comparer", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),),
              ),
            ],
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: ListView.builder(
                itemCount: myProducts.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 5.0),
                    height: 60.0,
                    child: InkWell(
                      onTap: () async {
                        var otherProduct = await APIClient.getFoodFact(myProducts[index].split(',')[1]);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ComparePage(product, otherProduct)),);
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: 50.0,
                            height: 50.0,
                            margin: EdgeInsets.only(right: 10.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: NetworkImage(myProducts[index].split(',')[2]),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Flexible(
                                      child: Text(myProducts[index].split(',')[0], style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.bold),),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Flexible(
                                      child: Text(myProducts[index].split(',')[4], style: TextStyle(color: Colors.grey, fontSize: 14.0),),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                }
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    MaterialButton(
                      onPressed: () async {
                        compareScan();
                      },
                      child: Text("SCANNER UN PRODUIT", style: TextStyle(color: Colors.white),),
                    ),
                    MaterialButton(
                      onPressed: () async {
                        Navigator.of(context).pop();
                      },
                      child: Text("ANNULER", style: TextStyle(color: Colors.white),),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future compareScan() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WimplateDialog(
          Container(
            height: 100.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );

    try {
      final barcode = await QrUtils.scanQR;
      print(barcode);

      if(barcode != null) {
        var otherProduct = await APIClient.getFoodFact(barcode);

        Navigator.of(context).pop();

        if(otherProduct.getStatus() == 0) {
          _scaffoldKey.currentState.showSnackBar(
              SnackBar(
                content: Text("Aucun produit ne correspond au code bar", style: TextStyle(color: Colors.redAccent),),
                action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
                backgroundColor: Colors.black.withOpacity(0.8),
              )
          );
          print("Produit non trouvé");
        } else {

          SharedPreferences prefs = await SharedPreferences.getInstance();


          print("Produit trouvé");
          var history = prefs.getString("list0");
          if(!history.contains(barcode)) {
            UserdataController.addProductToList(otherProduct, "list0");
          }
          reloadPrefs();

          Navigator.push(context, MaterialPageRoute(builder: (context) => ComparePage(product, otherProduct)),);
          //print("Nav passed");
        }
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text("Aucun code bar détecté", style: TextStyle(color: Colors.redAccent),),
            action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
            backgroundColor: Colors.black.withOpacity(0.8),
          ),
        );
      }
    } on PlatformException catch (e) {
      print(e);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Erreur : $e", style: TextStyle(color: Colors.redAccent),),
        ),
      );
      print(e);
    }
  }

}