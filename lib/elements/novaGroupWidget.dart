
import 'package:flutter/material.dart';

class NovaGroupWidget extends StatelessWidget {

  final int group;
  final double width;

  NovaGroupWidget(this.group, this.width);

  List<Color> colors = [Colors.green, Colors.greenAccent, Colors.orangeAccent, Colors.deepOrangeAccent, Colors.redAccent];

  List<String> descriptions = ["Aliments non transformés ou transformés minimalement", "Ingrédients culinaires transformés", "Aliments transformés", "Produits alimentaires et boissons ultra-transformés"];

  @override
  Widget build(BuildContext context) {
    return group != null ? Container(
        width: this.width + 20.0,
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10.0, top: 10.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: this.width / 4,
                        height: group == 1 ? 50.0 : 2.0,
                        color: Colors.green,
                        child: Center(
                          child: group == 1 ? Text("1", style: TextStyle(color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.bold),) : Container(),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: this.width / 4,
                        height: group == 2 ? 50.0 : 2.0,
                        color: Colors.yellow,
                        child: Center(
                          child: group == 2 ? Text("2", style: TextStyle(color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.bold)) : Container(),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: this.width / 4,
                        height: group == 3 ? 50.0 : 2.0,
                        color: Colors.orange,
                        child: Center(
                          child: group == 3 ? Text("3", style: TextStyle(color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.bold)) : Container(),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: this.width / 4,
                        height: group == 4 ? 50.0 : 2.0,
                        color: Colors.red,
                        child: Center(
                          child: group == 4 ? Text("4", style: TextStyle(color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.bold)) : Container(),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Text(descriptions[group - 1], style: TextStyle(color: Colors.grey, fontSize: 18.0, fontStyle: FontStyle.italic)),
                  )
                ],
              ),
            )
          ],
        )
    ) : Container(
      width: this.width + 20.0,
      height: 60.0,
      child: Center(
        child: Text("Groupe Nova introuvable", style: TextStyle(color: Colors.grey, fontSize: 14.0, fontStyle: FontStyle.italic),),
      ),
    );
  }

}