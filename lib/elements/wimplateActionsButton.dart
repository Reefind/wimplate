
import 'package:flutter/material.dart';

import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:wimplate/core/apiModels.dart';
import 'package:wimplate/core/wimplateActionsController.dart';

class WimplateActionsButton extends StatelessWidget {

  final OpenFoodFact product;
  final BuildContext context;
  final Function reloadPrefs;
  final GlobalKey<ScaffoldState> _scaffoldKey;

  WimplateActionsButton(this.product, this.context, this.reloadPrefs, this._scaffoldKey);

  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      marginRight: 18,
      marginBottom: 20,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      // this is ignored if animatedIcon is non null
      // child: Icon(Icons.add),
      visible: true,
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      tooltip: 'Actions',
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: Colors.white,
      foregroundColor: Colors.black,
      elevation: 8.0,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
          child: Icon(Icons.add_shopping_cart, color: Colors.black,),
          backgroundColor: Colors.white.withOpacity(0.9),
          label: 'Je le prends',
          labelBackgroundColor: Colors.white.withOpacity(0.6),
          labelStyle: TextStyle(fontSize: 14.0),
          onTap: () {
            WimplateActionsController.takeProduct(product, context, _scaffoldKey, reloadPrefs);
          },
        ),
        SpeedDialChild(
          child: Icon(Icons.add, color: Colors.black,),
          backgroundColor: Colors.white.withOpacity(0.9),
          label: 'Ajouter à une liste',
          labelBackgroundColor: Colors.white.withOpacity(0.6),
          labelStyle: TextStyle(fontSize: 14.0),
          onTap: () {
            WimplateActionsController.addToList(context, reloadPrefs, product, _scaffoldKey);
          },
        ),
        SpeedDialChild(
          child: Icon(Icons.compare_arrows, color: Colors.black,),
          backgroundColor: Colors.white.withOpacity(0.9),
          label: 'Comparer',
          labelBackgroundColor: Colors.white.withOpacity(0.6),
          labelStyle: TextStyle(fontSize: 14.0),
          onTap: () {
            WimplateActionsController.compare(product, context, _scaffoldKey, reloadPrefs);
          },
        ),
        SpeedDialChild(
          child: Icon(Icons.edit, color: Colors.black,),
          backgroundColor: Colors.white.withOpacity(0.9),
          label: 'Editer',
          labelBackgroundColor: Colors.white.withOpacity(0.6),
          labelStyle: TextStyle(fontSize: 14.0),
          onTap: () {
            _scaffoldKey.currentState.showSnackBar(
              SnackBar(
                content: Text("Cette fonctionnalité sera bientôt disponible", style: TextStyle(color: Colors.white),),
                action: SnackBarAction(label: "Fermer", textColor: Colors.white, onPressed: () { }),
                backgroundColor: Colors.black.withOpacity(0.8),
              ),
            );
          },
        ),
      ],
    );
  }
}