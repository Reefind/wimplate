

import 'package:flutter/material.dart';
import 'package:wimplate/core/apiModels.dart';

class NutritionMarkersWidget extends StatelessWidget {

  final OpenFoodFact product;

  NutritionMarkersWidget(this.product);

  Color getMarkerColor(String value) {
    switch(value) {
      case "low": return Colors.greenAccent; break;
      case "moderate": return Colors.orangeAccent; break;
      case "high": return Colors.redAccent; break;
      default: return Colors.white; break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 15.0),
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 20.0,
                  height: 20.0,
                  margin: EdgeInsets.only(right: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: getMarkerColor(product.getFatScore()),
                  ),
                ),
                Text("Matières grasses", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 15.0),
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 20.0,
                  height: 20.0,
                  margin: EdgeInsets.only(right: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: getMarkerColor(product.getSaturatedFatScore()),
                  ),
                ),
                Text("Acides gras saturés", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 15.0),
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 20.0,
                  height: 20.0,
                  margin: EdgeInsets.only(right: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: getMarkerColor(product.getSugarScore()),
                  ),
                ),
                Text("Sucre", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 15.0),
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 20.0,
                  height: 20.0,
                  margin: EdgeInsets.only(right: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    color: getMarkerColor(product.getSaltScore()),
                  ),
                ),
                Text("Sel", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
              ],
            ),
          ),
        ]
    );
  }

}