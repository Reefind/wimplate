import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/services.dart';

import 'package:rubber/rubber.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wimplate/core/wimplateActionsController.dart';
import 'package:wimplate/elements/compareDialogBody.dart';
import 'package:wimplate/elements/nutritionFactsWidget.dart';
import 'package:wimplate/elements/nutritionMarkersWidget.dart';
import 'package:wimplate/elements/wimplateActionsButton.dart';

import 'core/additivesController.dart';
import 'core/apiModels.dart';
import 'elements/novaGroupWidget.dart';
import 'elements/nutritionGradeWidget.dart';
import 'elements/wimplateDialog.dart';

class ProductPage extends StatefulWidget {

  final OpenFoodFact product;
  final String code;
  final Function reloadPrefs;

  ProductPage(this.product, this.code, this.reloadPrefs);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> with SingleTickerProviderStateMixin {

  RubberAnimationController _rubberController;

  ScrollController _scrollController = ScrollController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool additivesLoaded = false;
  List<dynamic> additives;
  List<int> additivesLevels;
  bool hasAdditives = false;

  @override
  void initState() {
    _rubberController = RubberAnimationController(
      vsync: this,
      lowerBoundValue: AnimationControllerValue(pixel: 500),
      duration: Duration(milliseconds: 200),
    );

    widget.reloadPrefs();

    additives = widget.product.getAdditives();
    loadAdditives();

    _scrollController.addListener(_scrollListener);

    _rubberController.addStatusListener(_rubberStatusListener);
    _rubberController.animationState.addListener(_rubberStateListener);
    super.initState();
  }

  @override
  void dispose() {
    _rubberController.animationState.removeListener(_rubberStateListener);
    _rubberController.removeStatusListener(_rubberStatusListener);
    super.dispose();
  }

  void loadAdditives() async {
    if(additives == null || additives.length == 0) {
      print("No additives found.");
    } else {
      print("Found ${additives.length} additives.");

      additivesLevels = await getAdditivesLevel(additives);

      setState(() {
        hasAdditives = true;
      });
    }
  }

  Future<List<int>> getAdditivesLevel(List<dynamic> additives) async {
    var data = await rootBundle.loadString('assets/additives.json');
    var additivesJson = json.decode(data);

    var result = List<int>();

    for(var id in additives) {
      print("Checking : " + id.toString());
      var level = additivesJson[id.toString()];
      if(level != null) {
        result.add(level as int);
      } else {
        result.add(4);
      }
    }

    setState(() {
      additivesLoaded = true;
    });

    return result;
  }

  void launchAdditiveInfo(String additive) async {
    String url = AdditivesController.getAdditiveLink(additive);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _rubberStateListener() {
    print("state changed ${_rubberController.animationState.value}");
  }

  void _rubberStatusListener(AnimationStatus status) {
    print("changed status ${_rubberController.status}");
  }

  void _rubberExpand() {
    _rubberController.expand();
  }

  void _rubberCollapse() {
    _rubberController.collapse();
  }

  Color getGlobalScoreColor(int score) {
    if(score < 25) {
      return Colors.redAccent;
    } else if(score < 50) {
      return Colors.orangeAccent;
    } else if(score < 75) {
      return Colors.yellowAccent;
    } else {
      return Colors.greenAccent;
    }
  }

  Color getLevelColors(int level) {
    switch(level) {
      case 0: return Colors.greenAccent; break;
      case 1: return Colors.yellowAccent; break;
      case 2: return Colors.orangeAccent; break;
      case 3: return Colors.redAccent; break;
      case 4: return Colors.white; break;
      default: return Colors.white; break;
    }
  }

  String getLevelText(int level) {
    switch(level) {
      case 0: return "Peu dangereux"; break;
      case 1: return "Ne pas en abuser"; break;
      case 2: return "Douteux"; break;
      case 3: return "Nocif"; break;
      case 4: return "Indéfini"; break;
      default: return "Indéfini"; break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromARGB(255, 55, 55, 71),
      floatingActionButton: WimplateActionsButton(widget.product, context, widget.reloadPrefs, _scaffoldKey),
      body: Container(
        child: RubberBottomSheet(
          lowerLayer: _getLowerLayer(),
          upperLayer: _getUpperLayer(),
          animationController: _rubberController,
          scrollController: _scrollController,
        ),
      ),
    );
  }

  _scrollListener() {
    if (_scrollController.offset <= _scrollController.position.minScrollExtent && !_scrollController.position.outOfRange) {
      print("Reached Top");
      _rubberCollapse();
    } else if(_rubberController.animationState.value != AnimationState.expanded) {
      print("Expand");
      _rubberExpand();
    }
  }

  Widget _getLowerLayer() {
    return Stack(
      children: <Widget>[
        Container(
          height: 320.0,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(widget.product.getIllustration()),
              fit: BoxFit.cover,
            ),
          ),
        ),
        widget.product.isOrganic() ? Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Transform.rotate(
              angle: 0.5,
              child: Container(
                width: 100.0,
                height: 100.0,
                margin: EdgeInsets.only(top: 40.0, right: 0.0),
                decoration: BoxDecoration(
                  color: Colors.green.withOpacity(0.9),
                  borderRadius: BorderRadius.all(Radius.circular(100.0)),
                  border: Border.all(color: Colors.white, width: 5.0),
                ),
                child: Center(
                  child: Text("BIO", style: TextStyle(color: Colors.white, fontSize: 32.0, fontWeight: FontWeight.bold),),
                ),
              ),
            ),
          ],
        ) : Container(),
        Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 25.0, left: 5.0),
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                }
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _getUpperLayer() {
    return ClipRRect(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 2.0,
          sigmaY: 2.0,
        ),
        child: Container(
          padding: EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
            color: Color(0xFF1f2021).withOpacity(0.95),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: _rubberExpand,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 100.0,
                          height: 4.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4.0)),
                            color: Colors.white.withOpacity(0.2),
                          ),
                          margin: EdgeInsets.only(top: 2.0, bottom: 10.0),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ScrollConfiguration(
                      behavior: ScrollBehaviorNoGlow(),
                      child: ListView(
                          controller: _scrollController,
                          //shrinkWrap: true,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 15.0, top: 0.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Text(widget.product.getName(), style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 15.0, top: 2.0, bottom: 5.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Text("${widget.product.getGenericName()} ", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal, fontStyle: FontStyle.italic), textAlign: TextAlign.start,),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 15.0, bottom: 20.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text("Commercialisé par " + widget.product.getBrand(), style: TextStyle(color: Colors.grey, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.25,
                                    height: MediaQuery.of(context).size.width * 0.25,
                                    margin: EdgeInsets.only(left: 15.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                                      image: DecorationImage(image: NetworkImage(widget.product.getImageUrl()), fit: BoxFit.cover),
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                Column(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.4,
                                      margin: EdgeInsets.only(left: 25.0, bottom: 5.0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Flexible(
                                            child: Text("Nutri-score", style: TextStyle(color: Colors.white, fontStyle: FontStyle.normal, fontSize: 18.0),),
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20.0),
                                      child: NutritionGradeWidget(widget.product.getNutritionGrade(), MediaQuery.of(context).size.width / 2),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              padding: EdgeInsets.only(left: 15.0, top: 15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text("Groupe Nova : ", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20.0),
                              child: NovaGroupWidget(widget.product.getNovaGroup(), MediaQuery.of(context).size.width * 0.8),
                            ),
                            hasAdditives ? Container(
                              margin: EdgeInsets.only(top: 10.0),
                              padding: EdgeInsets.all(15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text("Additifs : ", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                ],
                              ),
                            ) : Container(),
                            hasAdditives ? ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: additives.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    margin: EdgeInsets.only(left: 15.0),
                                    padding: EdgeInsets.all(5.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              width: 20.0,
                                              height: 20.0,
                                              margin: EdgeInsets.only(right: 10.0),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                                                color: getLevelColors(additivesLevels[index]),
                                              ),
                                            ),
                                            Text(additives[index], style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                            Container(
                                              width: 10.0,
                                            ),
                                            Text(AdditivesController.getAdditiveFunction(additives[index]), style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),

                                          ],
                                        ),
                                        Container(
                                          child: Center(
                                            child: GestureDetector(
                                              onTap: () {
                                                launchAdditiveInfo(additives[index]);
                                              },
                                              child: Icon(Icons.info_outline, color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }
                            ): Container(),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              padding: EdgeInsets.all(15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text("Repères nutritionnels :", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                ],
                              ),
                            ),
                            NutritionMarkersWidget(widget.product),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              padding: EdgeInsets.all(15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Text("Informations nutritionnels (100g / AJR) :", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                  ),
                                ],
                              ),
                            ),
                            NutritionFactsWidget(widget.product),
                            Container(
                              margin: EdgeInsets.only(top: 25.0),
                              padding: EdgeInsets.only(left: 15.0, right: 15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text("Ingrédients :", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 0.0, bottom: 60.0),
                              padding: EdgeInsets.all(15.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Text(widget.product.getIngredients(), style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                  ),
                                ],
                              ),
                            ),
                            /*Container(
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: widget.product.getOrderedIngredients().length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                                padding: EdgeInsets.all(5.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(widget.product.getOrderedIngredients()[index].getText() + " ${widget.product.getOrderedIngredients()[index].getPercentage()}",
                                      style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              );
                            }
                          ),
                        ),*/
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
    );
  }
}

class ScrollBehaviorNoGlow extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}