
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

import 'core/additivesController.dart';
import 'core/apiModels.dart';
import 'elements/nutritionGradeWidget.dart';

class ComparePage extends StatefulWidget {

  final OpenFoodFact product1;
  final OpenFoodFact product2;

  ComparePage(this.product1, this.product2);

  @override
  _ComparePageState createState() => _ComparePageState();
}

class _ComparePageState extends State<ComparePage> {

  List<dynamic> additives1;
  List<dynamic> additives2;
  List<int> additivesLevels1;
  List<int> additivesLevels2;

  bool hasAdditives1 = false;
  bool hasAdditives2 = false;

  @override
  void initState() {
    super.initState();
    additives1 = widget.product1.getAdditives();
    additives2 = widget.product2.getAdditives();

    loadAdditives();
  }


  void loadAdditives() async {
    additivesLevels1 = await getAdditivesLevel(additives1);
    additivesLevels2 = await getAdditivesLevel(additives1);

    print("LevelRetreived");

    setState(() {
      hasAdditives1 = additives1.length != 0;
      hasAdditives2 = additives2.length != 0;
    });
  }

  Future<List<int>> getAdditivesLevel(List<dynamic> additives) async {
    var data = await rootBundle.loadString('assets/additives.json');
    var additivesJson = json.decode(data);

    print("Retreiving Levels");

    var result = List<int>();

    for(var id in additives) {
      print("Checking : " + id.toString());
      var level = additivesJson[id.toString()];
      if(level != null) {
        result.add(level as int);
      } else {
        result.add(4);
      }
    }

    return result;
  }

  Color getLevelColors(int level) {
    switch(level) {
      case 0: return Colors.greenAccent; break;
      case 1: return Colors.yellowAccent; break;
      case 2: return Colors.orangeAccent; break;
      case 3: return Colors.redAccent; break;
      case 4: return Colors.white; break;
      default: return Colors.white; break;
    }
  }

  void launchAdditiveInfo(String additive) async {
    String url = AdditivesController.getAdditiveLink(additive);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0f0f0f),
    appBar: AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
    ),
      body: ListView(
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 80.0,
                          height: 80.0,
                          margin: EdgeInsets.only(bottom: 20.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: NetworkImage(widget.product1.getImageUrl()), fit: BoxFit.cover),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Text(widget.product1.getName(), style: TextStyle(color: Colors.white, fontSize: 18.0), textAlign: TextAlign.center,),
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Text(widget.product1.getBrand(), style: TextStyle(color: Colors.grey, fontSize: 18.0, fontStyle: FontStyle.italic)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 80.0,
                          height: 80.0,
                          margin: EdgeInsets.only(bottom: 20.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: NetworkImage(widget.product2.getImageUrl()), fit: BoxFit.cover),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Text(widget.product2.getName(), style: TextStyle(color: Colors.white, fontSize: 18.0), textAlign: TextAlign.center,),
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Text(widget.product2.getBrand(), style: TextStyle(color: Colors.grey, fontSize: 18.0, fontStyle: FontStyle.italic)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Text("Nutri-score :", style: TextStyle(color: Colors.white, fontSize: 20.0),),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Center(
                  child: NutritionGradeWidget(widget.product1.getNutritionGrade(), MediaQuery.of(context).size.width * 0.35),
                ),
                Center(
                  child: NutritionGradeWidget(widget.product2.getNutritionGrade(), MediaQuery.of(context).size.width * 0.35),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Text("Additifs :", style: TextStyle(color: Colors.white, fontSize: 20.0),),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width / 2.0,
                  child: hasAdditives1 ? ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: additives1.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.only(left: 15.0),
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 20.0,
                                    height: 20.0,
                                    margin: EdgeInsets.only(right: 10.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                                      color: getLevelColors(additivesLevels1[index]),
                                    ),
                                  ),
                                  Text(additives1[index], style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                  Container(
                                    width: 10.0,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.22,
                                    child: Text(
                                      AdditivesController.getAdditiveFunction(additives1[index]),
                                      style: TextStyle(color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.normal),
                                      textAlign: TextAlign.start,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                child: Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      launchAdditiveInfo(additives1[index]);
                                    },
                                    child: Icon(Icons.info_outline, color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                  ): Container(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2.0,
                  child: hasAdditives2 ? ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: additives2.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.only(left: 15.0),
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 20.0,
                                    height: 20.0,
                                    margin: EdgeInsets.only(right: 10.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                                      color: getLevelColors(additivesLevels2[index]),
                                    ),
                                  ),
                                  Text(additives2[index], style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                                  Container(
                                    width: 10.0,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.22,
                                    child: Text(
                                      AdditivesController.getAdditiveFunction(additives2[index]),
                                      style: TextStyle(color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.normal),
                                      textAlign: TextAlign.start,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                child: Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      launchAdditiveInfo(additives2[index]);
                                    },
                                    child: Icon(Icons.info_outline, color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                  ): Container(),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Text("Informations nutritionnels (pour 100g) :", style: TextStyle(color: Colors.white, fontSize: 20.0),),
                )
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Énergie", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsEnergy()} kCal", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsEnergy()} kCal", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Matières grasses", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsFat()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsFat()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Acides gras saturés", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsSaturatedFats()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsSaturatedFats()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Glucides", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsCarbohydrates()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsCarbohydrates()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Sucres", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsSugar()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsSugar()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Sel", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsSalt()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsSalt()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Fibres", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsFiber()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsFiber()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Protéines", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 20.0, bottom: 20.0),
                padding: EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text("${widget.product1.getNutrimentsProteins()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                    Text("${widget.product2.getNutrimentsProteins()} g", style: TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.normal), textAlign: TextAlign.start,),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

}