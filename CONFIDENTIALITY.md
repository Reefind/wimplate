# Régles de confidentialités.

Aucune donnée utilisée par l'application Wimplate ne sort de votre appareil.
Vous seul avez accès à vos données et aucune information personnelle n'est transmise à un serveur.

Seuls les codes barres scannés sont utilisés afin d'appeler l'API OpenFoodFacts (https://fr.openfoodfacts.org/)

Les données affichés sont fournis par OpenFoodFacts.
Les informations sont remplis par la communauté et ne peuvent être considérés comme véridiques.