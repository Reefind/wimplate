# Wimplate FR

Wimplate est un concurrent à Yuka, open source et 100% transparent.

### Screenshots
<details>
  <summary>Home Page</summary>
    ![Homepage](https://lh3.googleusercontent.com/pw/AM-JKLWNnpGYlwVDGC718zbt7RWSI-cQZViLgc24HruXW_TKjljOndka37smi_hS6rQ7FDApDUc_pozyzlNr_7p2atnnWchRNgofK6HBZbs0Z24xlAM-bFMbT5QXuBp71jk36byURiUpCeiugG8GsCswhW_seg=w595-h1289-no?authuser=0)
</details>
<details>
  <summary>Personal Page</summary>
    ![Personal page](https://lh3.googleusercontent.com/pw/AM-JKLV3sZvPpsGkkJ_GjZ1dU-x4z2gfgo_nGjAMMCfLza9biP_yg5eQBuDoGSrtaHJROT2xbqIBhaYHMfVkPFWJITiB5wOS2t5WIiFHMYotwmLkBB160EHdPHJR_POThrM2IcpOOSzfRNd_ySMwH6qG9Max4A=w595-h1289-no?authuser=0)
</details>
<details>
  <summary>Product Page</summary>
    ![Product Page](https://lh3.googleusercontent.com/pw/AM-JKLUEbUWXNBlRlbltil-9kjFVcQP-lR6bL12crpM6yB9tEp4cOAqVuYCmII4ni8Kt6FR6QrAxCUtNmEtlC5LcHuyDYrXhmB502Upf3GwE1t6EUZZCThS2o04FRMD0c36zbG0EySG0jd21CZpKq4rfo-Ch-w=w595-h1289-no?authuser=0)
</details>
<details>
  <summary>Product Page Expanded</summary>
    ![Product Page Expanded](https://lh3.googleusercontent.com/pw/AM-JKLUkoApetXTKWyBqxpHlb666g3vA5BMe49yDW-ZY6tthXRrueBX-TZtSQFBojnFcK8j1s9NMhyWJ2GBROiGKYZJ8jh2tzvvRQ75qcyufVVEdnQVfbHswaUQV0pFarBbufsfgwDyQsIt_Orvd3XjsuYOhXQ=w595-h1289-no?authuser=0)
</details>

## Update : I joined Open Food Facts to continue the work with them. If you were interested in Wimplate you can join the #smooth-app channel in the Open Food Facts's Slack.
### Notice : I'm currently helping to the development of this plugin https://github.com/openfoodfacts/openfoodfacts-dart. Once it reaches a stable state Wimplate's logic will be re-written with it.

## Le projet

Wimplate utilise exclusivement l'API OpenFoodFacts et des illustrations Unsplash.

Le but de cette application est de fournir aux utilisateurs une experience irréprochable.
Pour cela Wimplate n'inclut aucune publicité et ne requiert aucune inscription.
Toutes les données utilisées sont libres et directement consultables sur https://fr-en.openfoodfacts.org/

A terme l'application permettra aux utilisateurs d'alimenter eux même la base de données OpenFoodFacts depuis Wimplate.

### Done

*  Recherche de poduit par code bar
*  Affichage d'un produit (Repères nutritionnels, Informations nutritionnelkes, Ingrédients)
*  Affichage de l'illustration en fonction de la catégorie de produit & la photo du produit
*  Compte du nombre de produits scannés
*  Créer des listes personnalisées
*  Ajouter des produits à une liste
*  Historique des produits scannés
*  Ajuster la jauge Wims en fonction de la qualité des produits ajoutés
*  Comparer deux produits

### TODO

*  Informations sur les allergènes
*  Ajouter un produit à la base de données OpenFoodFacts
*  Transition vers une base de données SQL
*  Créer des groupes de produits (recettes / menu) affichant un résumé des apports d'un groupe
*  ? Déterminer si le produit est végétarien / vegan ?


## Pour les développeurs

#### L'application est developpée en Dart avec le SDK Flutter.

### Etat du code

~~Le code est à l'état de prototype (Les pages sont écrites dans une seule classe, les widgets réutilisables doivent être séparés en plusieurs classes)~~
Le code source a été re-structuré pour facilité sa lisibilité.

### Stockage des données

#### Note : Wimplate est en cours de transition vers une base de données SQL afin de stocker plus de données et fournir un mode hors ligne.

L'intégralité des données sont stockées sur l'appareil.
La librairie shared_preferences (https://pub.dev/packages/shared_preferences) est utilisée.

| Variables | Type | Clef | Commentaire |
|---|---|---|---|
| Compteur de Wims | Int | wims |   |
| Listes de listes | String | myLists | nom0,identifiant0,illustration0;nom1,identifiant1,illustration1 |
| Liste de produits | String | 'listID' | La clef dépend de la liste et est stockée dans myLists, nom_produit0,codebar0,thumbnail0,illustration0,nutriscore0;... |



# Wimplate EN

Wimplate is a free and open source products scanning application.

## The project

Wimplate relies on API OpenFoodFacts's API and Unsplash illustrations.

This application aims to provide the best user experience combined with no personal data collection and full transparency.
That's why Wimplate doesn't include any ads nor require any registration.
The whole products data base is available here : https://fr-en.openfoodfacts.org/ (French database ATM).

In the future Wimplate will allow users to edit the OpenFoodFacts database directly from the app.

### Done

*  Scan a barcode and find the product
*  Show product page
*  Match the product with its category illustration
*  Count the number of scanned products
*  Create custom lists
*  Add products to a list
*  History
*  Wims progress bar
*  Compare two products

### TODO

*  Show more about allergens
*  Allow the user to edit the OpenFoodFact database
*  Transition to an SQL database
*  Add products groups (recipes / menus) and show a summary of the nutritional intake
*  ? Find out if a product is vegetarian / vegan ?


## For developers

#### The app is developed in Dart using the Flutter SDK.

### Code state

~~The code is pretty mush prototyping state (All pages are written in a single class, reusable widgets should be separated in multiple classes)~~
The source code has been re-structured to make it easier to understand.

### Data storage

#### Note : Wimplate is currently transitioning to an SQL data base to store more data and provide an offline mode.

All the data is stored on the device itself
The library shared_preferences (https://pub.dev/packages/shared_preferences) is used for that.

| Variables | Type | Key | Comment |
|---|---|---|---|
| Wims counter | Int | wims |   |
| Lists of listes | String | myLists | name0,id0,illustration0;name1,id1,illustration1 |
| Lists of products | String | 'listID' | The key depends on the list and is stored in myLists, product_name0,barcode0,thumbnail0,illustration0,nutritionscore0;... |

